using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class votingResults : MonoBehaviour
{   
    GameObject[] startdebGO;
    StartDebate startdeb;
    string code = "";
    float timeInterval = .5f;
    public bool started;

    public int blueVotes = 0, redVotes = 0;

    GameObject[] internetStatusGO;
    InternetStatus internetStatus; 
    public void setCode(){
        startdebGO = GameObject.FindGameObjectsWithTag("StartDebate");
        startdeb = startdebGO[0].GetComponent<StartDebate>();
        code = startdeb.code;
        Debug.Log("votingResults");
        internetStatusGO = GameObject.FindGameObjectsWithTag("InternetStatus");
        internetStatus = internetStatusGO[0].GetComponent<InternetStatus>();
    }
    void Update()
    {
        if(Time.time >= timeInterval && started && internetStatus.internetStatus){
            StartCoroutine(GetRequest("https://debatox.live/unity_db/readResults.php?kod=" + code));
            timeInterval=Mathf.FloorToInt(Time.time) + 1;

            GameObject[] VotesBlueGO = GameObject.FindGameObjectsWithTag("VotesBlue");
            Text VotesBlue = VotesBlueGO[0].GetComponent<Text>();
            VotesBlue.text = blueVotes.ToString();

            GameObject[] VotesRedGO = GameObject.FindGameObjectsWithTag("VotesRed");
            Text VotesRed = VotesRedGO[0].GetComponent<Text>();
            VotesRed.text = redVotes.ToString();
        }
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    //Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    //Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    //Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);

                    JsonContent jsonCon = new JsonContent();
                    jsonCon = JsonUtility.FromJson<JsonContent>(webRequest.downloadHandler.text);

                    blueVotes = jsonCon.propozycja_glosy;
                    redVotes = jsonCon.opozycja_glosy;

                    Debug.Log("blueVotes: " + blueVotes + " redVotes: " + redVotes);
                    
                    break;
            }
        }
    }
}
