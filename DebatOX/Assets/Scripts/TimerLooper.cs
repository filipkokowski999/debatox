using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerLooper : MonoBehaviour
{
    GameObject[] TimerGO;
    TMP_Text TimerTxt;

    public void loop(int minutes, int seconds, bool loop, bool firstPlay, int securedTime, bool looped, int count, int index, bool propozycja, Timer timerClass, bool soundsOn, bool pause, bool soundPlayed)
    {

        GameObject[] poparcie = GameObject.FindGameObjectsWithTag("ZaTematem");
        GameObject[] opozycja = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        if (count < poparcie.Length + opozycja.Length)
        {
            TimerGO = GameObject.FindGameObjectsWithTag("Timer");
            Timer timer = TimerGO[0].AddComponent<Timer>();
            timer.values(minutes, seconds, loop, firstPlay, securedTime, looped, count, index, propozycja, false, true, soundsOn, pause, soundPlayed);
            timer.looped = false;

            Destroy(timerClass);

            GameObject[] TimerGOTxt = GameObject.FindGameObjectsWithTag("Timer");
            TimerGOTxt[0].transform.localPosition = new Vector3(-2.5f, -21, 0);
        }
        else
        {
            TimerGO = GameObject.FindGameObjectsWithTag("Timer");
            Timer timer = TimerGO[0].AddComponent<Timer>();
            timer.values(minutes, seconds, loop, firstPlay, securedTime, looped, count, index, propozycja, true, true, soundsOn, pause, soundPlayed);
            timer.looped = false;

            Destroy(timerClass);

            GameObject[] TimerGOTxt = GameObject.FindGameObjectsWithTag("Timer");
            TimerGOTxt[0].transform.localPosition = new Vector3(-2.5f, -21, 0);
        }

        Destroy(GetComponent<TimerLooper>());
        Destroy(GetComponent<Timer>());
    }

}
