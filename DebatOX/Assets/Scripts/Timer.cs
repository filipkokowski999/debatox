using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Timer : MonoBehaviour
{

    GameObject[] timer;
    public float timeValue;
    float timeValueAtStart;
    public int minutes, seconds;

    bool started = false;
    TMP_Text timerText;

    GameObject[] sand;

    int count = 0;

    /*Vector3 defHourglassDim;

    GameObject[] particleSystem;
    ParticleSystem particle;

    GameObject[] Hourglass;*/

    public bool looped = false;

    public bool loop = false;

    public bool firstPlay = true;

    public bool buttonClicked = false;

    public int securedTime;

    public bool inSecuredTime = false;

    public int countLoop;

    public string timeString;
    public int time;

    GameObject[] controlButtons, controls;
    Button controlButton;

    GameObject[] debateProcGO;
    public int index;

    GameObject[] playSoundGO;
    playSound sound;

    GameObject[] nextPersonGO;
    nextPerson next;

    public bool propozycja;
    public bool lastLoop;

    public bool nextPerson;

    GameObject[] poparcie;
    GameObject[] opozycja;

    int poparcieLenght;
    int opozycjaLength;

    bool blinking;

    public bool pause;
    public bool playS;
    public bool endDebate;
    public int skipped = 0;
    public bool saveTime;
    public int sec;

    public bool prev;
    public bool soundsOn;
    
    public bool paused; 

    bool soundPlayedTimer;
    public bool soundPlayed;

    public void values(int minutes, int seconds, bool loop, bool firstPlay, int securedTime, bool looped, int count, int index, bool propozycja, bool lastLoop, bool playS, bool soundsOn, bool pause, bool soundPlayed)
    {
        this.minutes = minutes;
        this.seconds = seconds;
        this.loop = loop;
        this.firstPlay = firstPlay;
        this.securedTime = securedTime;
        this.countLoop = count;
        this.index = index;
        this.propozycja = propozycja;
        this.lastLoop = lastLoop;
        this.playS = playS;
        this.soundsOn = soundsOn;
        this.pause = pause;
        this.soundPlayed = soundPlayed;

        GameObject[] previousGO = GameObject.FindGameObjectsWithTag("Prev");
        previousPerson previous = previousGO[0].GetComponent<previousPerson>();

        
        previous.addData(minutes,seconds,loop,firstPlay,securedTime,looped,countLoop,index,propozycja,lastLoop,playS,soundPlayed);
        Debug.Log("index add data: " + index + ", seconds:" + seconds);


        playSoundGO = GameObject.FindGameObjectsWithTag("PlaySound");
        sound = playSoundGO[0].GetComponent<playSound>();

        timeValue = (minutes * 60) + seconds + 1;
        timeValueAtStart = timeValue;
        //Debug.Log(timeValue);

        started = true;
        
        poparcie = GameObject.FindGameObjectsWithTag("ZaTematem");
        opozycja = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        poparcieLenght = poparcie.Length;
        opozycjaLength = opozycja.Length;

        controls = GameObject.FindGameObjectsWithTag("Controls");

        controlButtons = GameObject.FindGameObjectsWithTag("ControlButtons");

        controlButton = controlButtons[0].GetComponent<Button>();

        if (!looped) controls[0].transform.localPosition = new Vector3(-43.971f, 120.246f, 0);

        GameObject[] debateProcGO = GameObject.FindGameObjectsWithTag("DebateProc");
        Debate debateProc = debateProcGO[0].AddComponent<Debate>();
        //Debug.Log("Prev: " + prev);
        debateProc.debateProcess(seconds, index, propozycja, lastLoop, prev);
        //Debug.Log("Index=" + index);
        this.index++;
        prev = false;

    }

    void Start()
    {
        timer = GameObject.FindGameObjectsWithTag("Timer");
        TMP_Text TimerText = timer[0].GetComponent<TMP_Text>();

        timerText = TimerText;
        
    }

    void Update()
    {

        if(!endDebate){

            if (started && !lastLoop && !paused)
            {
                timeValue -= Time.deltaTime;
                //Debug.Log(timeValue);
                if (timeValue > 0)
                {
                    if (timeValue <= securedTime + 1)
                    {
                        if (!inSecuredTime && securedTime != 0)
                        {
                            GameObject[] timer = GameObject.FindGameObjectsWithTag("Timer");
                            TMP_Text timerTxt = timer[0].GetComponent<TMP_Text>();
                            timerTxt.color = new Color32(255, 0, 0, 255);
                            inSecuredTime = true;
                            Debug.Log("playS2: " + playS+ "::" + index);

                            GameObject[] menuGO = GameObject.FindGameObjectsWithTag("MenuController");
                            MenuScript menu = menuGO[0].GetComponent<MenuScript>();
                            if(playS && menu.soundsOn && !soundPlayed){
                               sound.playSoundTime();
                               soundPlayedTimer = true;
                            }
                        }

                    }
                    else
                    {
                        GameObject[] timer = GameObject.FindGameObjectsWithTag("Timer");
                        TMP_Text timerTxt = timer[0].GetComponent<TMP_Text>();
                        timerTxt.color = new Color32(0, 0, 0, 255);
                    }
                    time = (int)timeValue;

                    sec = time % 60;
                    //Debug.Log(sec);
                    int min = (time - sec) / 60;
                    string secStr = sec.ToString();
                    string minStr = min.ToString();


                    if (sec >= 10)
                    {
                        timeString = minStr + ":" + secStr;
                    }
                    else
                    {
                        timeString = minStr + ":0" + secStr;
                    }

                    timerText.text = timeString;

                }
                else
                {
                    //particle.Stop();
                    //controlButton.interactable = true;
                    //Debug.Log("aaaaa");



                    if (!looped && nextPerson)
                    {
                        //loopF();
                        nextPerson = false;
                    }
                }

                if(pause){
                    paused = true;
                }
            }
            else nextPerson = false;
        }
    }

    public void loopF()
    {
        countLoop++;
        if (countLoop <= poparcieLenght + opozycjaLength)
        {
            if(saveTime){
                GameObject[] previousctGO = GameObject.FindGameObjectsWithTag("Prev");
                previousPerson previousct = previousctGO[0].GetComponent<previousPerson>();
                previousct.changeTime(index, sec, soundPlayedTimer);
                Debug.Log("soundPlayedTimer:" + soundPlayedTimer);
                Debug.Log("Index:" + index);
                saveTime = false;
            }
            Debug.Log("skipped: " + skipped);
            
            GameObject[] TimerLoop = GameObject.FindGameObjectsWithTag("TimerLoop");

            TimerLooper timerLooper = TimerLoop[0].AddComponent<TimerLooper>();
            loop = !loop;
            propozycja = !propozycja;
            Debug.Log("Next Person Index: " + index);
            GameObject[] previousGO = GameObject.FindGameObjectsWithTag("Prev");
            previousPerson previous = previousGO[0].GetComponent<previousPerson>();
            if(previous.indexArr[index] == -1){
                timerLooper.loop(minutes, seconds, loop, firstPlay, securedTime, true, count, index, propozycja, this, soundsOn, pause, soundPlayed);
                Debug.Log("previous.indexArr[index]: " + previous.indexArr[index]);
            }
            else{
                Debug.Log("previous.indexArr[index]: " + previous.indexArr[index]);
                timerLooper.loop(
                        previous.minutesArr[index], previous.secondsArr[index],
                        previous.loopArr[index], previous.firstPlayArr[index], 
                        previous.securedTimeArr[index], true, 
                        previous.countArr[index], previous.indexArr[index], 
                        previous.propozycjaArr[index], this, 
                        soundsOn, pause, soundPlayed
                );
            }
            looped = true;
            //Debug.Log("Loop: " + loop + " Looped: " + looped);
        }
    }
}
