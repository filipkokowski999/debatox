using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public bool soundsOn = true;

    public void move(){
        GameObject[] menuBox = GameObject.FindGameObjectsWithTag("Menu");

        if(menuBox[0].transform.localPosition.y == 564){
            while(menuBox[0].transform.localPosition.y <= 565 && menuBox[0].transform.localPosition.y > -6){
                //menuBox[0].transform.localPosition -= new Vector3(0,5,0) * Time.deltaTime / 20;
                menuBox[0].transform.localPosition = new Vector3(0,-6,0);
            }
            //Debug.Log(menuBox[0].transform.localPosition.y + "brrrr");
        }
        else{
            while(menuBox[0].transform.localPosition.y < 564){
                menuBox[0].transform.localPosition += new Vector3(0,5,0) * Time.deltaTime;
            }
            menuBox[0].transform.localPosition = new Vector3(0,564,0);
            //Debug.Log(menuBox[0].transform.localPosition.y);
        }
    }   

    public void debatOX(){
        Application.OpenURL("https://debatox.live");
    }

    public void close(){
        Application.Quit();
    }

    public void soundSwitch(){
        soundsOn = !soundsOn;
    }

    public void voting(){
        
        GameObject[] votePaneGO = GameObject.FindGameObjectsWithTag("VoteQPaneController");
        votingPlane votePane = votePaneGO[0].GetComponent<votingPlane>();
        votePane.move();
    }

}
