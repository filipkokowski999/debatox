using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimeTxtField : MonoBehaviour
{
    public void onClick(int button)
    {
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("CzasyButton");
        GameObject[] txtFieldCzas = GameObject.FindGameObjectsWithTag("Czas");
        GameObject[] txtFieldCzasCh = GameObject.FindGameObjectsWithTag("CzasChroniony");


        if (button == 1)
        {
            txtFieldCzas[0].transform.localPosition = new Vector3(-4f, 305f, 80.53f);
            txtFieldCzasCh[0].transform.localPosition = new Vector3(-4f, 123.5f, 80.53f);
        }
        else if(button == 2)
        {
            txtFieldCzas[0].transform.localPosition = new Vector3(-4f, 123.5f, 80.53f);
            txtFieldCzasCh[0].transform.localPosition = new Vector3(-4f, 305f, 80.53f);
        }
    }
}
