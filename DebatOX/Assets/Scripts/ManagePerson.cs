using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ManagePerson : MonoBehaviour
{
    public TMP_InputField Strona;
    public GameObject prefab;
    public Transform parent;
    public Button button;
    public Button delete;
    private int screenHeight = Screen.height;

    public void Add()
    {
        if (Strona.tag == "ZaTematem")
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag("ZaTematem");

            if (objects.Length < 5)
            {
                for (int i = 1; i <= objects.Length; i++)
                {
                    if (i == objects.Length)
                    {

                        Vector3 pos = new Vector3(0, -screenHeight / 7, 0);

                        GameObject textField = Instantiate(prefab, objects[i - 1].transform.position + pos, Quaternion.identity, parent);
                        Vector3 textFilePos = textField.transform.localPosition;

                        Vector3 buttonPos = button.transform.localPosition;
                        buttonPos.y = textFilePos.y;
                        button.transform.localPosition = buttonPos;

                        Button deleteButton = Instantiate(delete, parent);

                        Vector3 deleteButtonPos = deleteButton.transform.localPosition;
                        deleteButtonPos.y = textFilePos.y;
                        deleteButtonPos.x += 30f;
                        deleteButton.transform.localPosition = deleteButtonPos;

                        deleteButton.name = "Delete";
                        deleteButton.GetComponentInChildren<Text>().text = i.ToString();

                        //Debug.Log(objects.Length);

                    }
                }
            }
            else
            {
                Debug.Log("Max uczestnik�w");
            }

        }
        else
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

            if (objects.Length < 5)
            {
                for (int i = 1; i <= objects.Length; i++)
                {
                    if (i == objects.Length)
                    { 

                        Vector3 pos = new Vector3(0, -screenHeight / 7, 0);

                        GameObject textField = Instantiate(prefab, objects[i - 1].transform.position + pos, Quaternion.identity, parent);
                        Vector3 textFilePos = textField.transform.localPosition;

                        Vector3 buttonPos = button.transform.localPosition;
                        buttonPos.y = textFilePos.y;
                        button.transform.localPosition = buttonPos;

                        Button deleteButton = Instantiate(delete, parent);

                        Vector3 deleteButtonPos = deleteButton.transform.localPosition;
                        deleteButtonPos.y = textFilePos.y;
                        deleteButtonPos.x -= 30f;
                        deleteButton.transform.localPosition = deleteButtonPos;

                        deleteButton.name = "Delete";
                        deleteButton.GetComponentInChildren<Text>().text = i.ToString();
                    }

                }
            }
            else
            {
                Debug.Log("Max uczestnik�w");
            }
        }
    }

    public void Delete(Button deleteButton)
    {
        if (Strona.tag == "ZaTematem")
        {

            GameObject[] deleteButtons = GameObject.FindGameObjectsWithTag("DeleteZaTematem");
            GameObject[] textFields = GameObject.FindGameObjectsWithTag("ZaTematem");

            //Debug.Log(deleteButtons.Length);
            //Debug.Log(textFields.Length);

            if (textFields.Length > 1)
            {
                for (int i = 0; i < deleteButtons.Length; i++)
                {

                        if (deleteButtons[i].GetComponentInChildren<Text>().text == deleteButton.GetComponentInChildren<Text>().text)
                        {

                            for (int x = textFields.Length-1; x > i; x--)
                            {
                                textFields[x].transform.localPosition = textFields[x-1].transform.localPosition;
                                deleteButtons[x].transform.localPosition = deleteButtons[x-1].transform.localPosition;
                                deleteButtons[x].GetComponentInChildren<Text>().text = (x - 1).ToString();
                            }

                            if (i == textFields.Length - 1)
                            {
                                Vector3 buttonPos = button.transform.localPosition;
                                Vector3 LastTextFieldPos = textFields[textFields.Length - 2].transform.localPosition;
                                buttonPos.y = LastTextFieldPos.y;
                                button.transform.localPosition = buttonPos;
                            }
                            else
                            {
                                Vector3 buttonPos = button.transform.localPosition;
                                Vector3 LastTextFieldPos = textFields[textFields.Length - 1].transform.localPosition;
                                buttonPos.y = LastTextFieldPos.y;
                                button.transform.localPosition = buttonPos;
                            }

                            Destroy(textFields[i]);
                            Destroy(deleteButtons[i]);
                            return;

                        } 
                }
            }
            else Debug.Log("Musi by� minimalnie jeden textField!");
        }
        else
        {
            GameObject[] deleteButtons = GameObject.FindGameObjectsWithTag("DeletePrzeciwkoTematowi");
            GameObject[] textFields = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

            if (textFields.Length > 1)
            {
                for (int i = 0; i < deleteButtons.Length; i++)
                {

                    if (deleteButtons[i].GetComponentInChildren<Text>().text == deleteButton.GetComponentInChildren<Text>().text)
                    {

                        for (int x = textFields.Length - 1; x > i; x--)
                        {
                            textFields[x].transform.localPosition = textFields[x - 1].transform.localPosition;
                            deleteButtons[x].transform.localPosition = deleteButtons[x - 1].transform.localPosition;
                            deleteButtons[x].GetComponentInChildren<Text>().text = (x - 1).ToString();
                        }


                        if (i == textFields.Length - 1)
                        {
                            Vector3 buttonPos = button.transform.localPosition;
                            Vector3 LastTextFieldPos = textFields[textFields.Length - 2].transform.localPosition;
                            buttonPos.y = LastTextFieldPos.y;
                            button.transform.localPosition = buttonPos;
                        }
                        else
                        {
                            Vector3 buttonPos = button.transform.localPosition;
                            Vector3 LastTextFieldPos = textFields[textFields.Length - 1].transform.localPosition;
                            buttonPos.y = LastTextFieldPos.y;
                            button.transform.localPosition = buttonPos;
                        }

                        Destroy(textFields[i]);
                        Destroy(deleteButtons[i]);
                        return;

                    }
                }
            }
            else Debug.Log("Musi by� minimalnie jeden textField!");
        }
    }
}
