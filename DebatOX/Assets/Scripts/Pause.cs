using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite pauseSprite;
    public Sprite playSprite;

    public void pause()
    {
        GameObject[] controlButtons = GameObject.FindGameObjectsWithTag("ControlButtons");
        Button controlButton = controlButtons[2].GetComponent<Button>();

        for(int i=0; i < controlButtons.Length; i++){
            controlButton = controlButtons[i].GetComponent<Button>();
            if(controlButton.GetComponentInChildren<Text>().text == "This") break;
        }

        GameObject[] timerGOClass = GameObject.FindGameObjectsWithTag("Timer");
        Timer timerClass = timerGOClass[0].GetComponent<Timer>();

        if (!timerClass.pause)
        {
            controlButton.image.sprite = playSprite;
            controlButton.image.rectTransform.sizeDelta = new Vector2(30,35);
            timerClass.pause = true;
        }
        else
        {
            controlButton.image.sprite = pauseSprite;
            controlButton.image.rectTransform.sizeDelta = new Vector2(30,30);
            timerClass.pause = false;
            timerClass.paused = false;
        }
       //Debug.Log(timerClass.pause);
    }

}
