using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class endDebate : MonoBehaviour
{

    GameObject[] timer;
    TMP_Text timerTxt;
    public Sprite pauseSprite;

    bool forceEnd;

    public void endButton(){
        forceEnd = true;
        resetScene();
    }

    public void resetScene()
    {
        GameObject[] TimeButtons = GameObject.FindGameObjectsWithTag("CzasyButton");
        GameObject[] TimerChGO = GameObject.FindGameObjectsWithTag("CzasChroniony");
        GameObject[] TimerGO = GameObject.FindGameObjectsWithTag("Czas");


        TimeButtons[0].transform.localPosition += new Vector3(0,-200, 0);
        TimeButtons[1].transform.localPosition += new Vector3(0, -200, 0);
        TimerGO[0].transform.localPosition = TimerChGO[0].transform.localPosition;
        TimerChGO[0].transform.localPosition += new Vector3(0, -200, 0);

        GameObject[] controlButtons = GameObject.FindGameObjectsWithTag("Controls");
        controlButtons[0].transform.localPosition = new Vector3(0, -221.37f, 0);

        GameObject[] StartButton = GameObject.FindGameObjectsWithTag("StartButton");
        Button Start = StartButton[0].GetComponent<Button>();
        StartButton[0].transform.localPosition += new Vector3(0, 300, 0);

        GameObject[] TitleTxtField = GameObject.FindGameObjectsWithTag("TematDebaty");
        TitleTxtField[0].transform.localPosition += new Vector3(0, -700, 0);
        TMP_InputField TitleIF = TitleTxtField[0].GetComponent<TMP_InputField>();
        TitleIF.readOnly = false;

        GameObject[] TematTxt = GameObject.FindGameObjectsWithTag("Temat");
        TematTxt[0].transform.localPosition += new Vector3(0,300,0);

        timer = GameObject.FindGameObjectsWithTag("Timer");
        timerTxt = timer[0].GetComponent<TMP_Text>();
        timerTxt.color = new Color32(0, 0, 0, 255);
        timerTxt.text = "0:00";

        GameObject[] zaTematem = GameObject.FindGameObjectsWithTag("ZaTematem");
        GameObject[] PrzeciwkoTematowi = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        foreach (GameObject textField in zaTematem)
        {
            TMP_InputField inputFieldBlue = textField.GetComponent<TMP_InputField>();
            inputFieldBlue.readOnly = false;
        }

        foreach (GameObject textField in PrzeciwkoTematowi)
        {
            TMP_InputField inputFieldRed = textField.GetComponent<TMP_InputField>();
            inputFieldRed.readOnly = false;
        }

        GameObject[] zaTematemDelete = GameObject.FindGameObjectsWithTag("DeleteZaTematem");
        GameObject[] PrzeciwkoTematowiDelete = GameObject.FindGameObjectsWithTag("DeletePrzeciwkoTematowi");

        for (int i = 1; i < zaTematemDelete.Length; i++) zaTematemDelete[i].transform.localPosition += new Vector3(300, 0, 0);

        for (int i = 1; i < PrzeciwkoTematowiDelete.Length; i++) PrzeciwkoTematowiDelete[i].transform.localPosition += new Vector3(-300, 0, 0);

        GameObject[] AddButtons = GameObject.FindGameObjectsWithTag("AddButton");
        foreach (GameObject button in AddButtons)
        {
            Button AddButton = button.GetComponent<Button>();
            AddButton.transform.localPosition += new Vector3(0, -700, 0);
        }

        GameObject[] timerGOClass = GameObject.FindGameObjectsWithTag("Timer");
        Timer timerClass = timerGOClass[0].GetComponent<Timer>();
        timerClass.countLoop = 0;
        timerClass.index = 1;
        timerClass.timeValue = 0f;
        timerClass.timeString = "00:00";
        timerClass.time = 0;
        timerClass.endDebate = true;
        timerClass.lastLoop = true;

        foreach (var comp in timerClass.GetComponents<Component>())
        {
            if (comp is Timer) Destroy(comp);
        }

        GameObject[] poparcieGO = GameObject.FindGameObjectsWithTag("ZaTematem");
        GameObject[] opozycjaGO = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        TMP_InputField[] poparcie = new TMP_InputField[poparcieGO.Length];
        TMP_InputField[] opozycja = new TMP_InputField[opozycjaGO.Length];

        for (int i = 0; i < poparcieGO.Length; i++)
        {
            poparcie[i] = poparcieGO[i].GetComponent<TMP_InputField>();
        }

        for (int i = 0; i < opozycjaGO.Length; i++)
        {
            opozycja[i] = opozycjaGO[i].GetComponent<TMP_InputField>();
        }

        foreach (TMP_InputField inputF in poparcie)
        {
            inputF.image.color = new Color32(146, 170, 255, 255);
            inputF.transform.localScale = new Vector3(1.379219f,1.5125f,1f);
        }

        foreach (TMP_InputField inputF in opozycja)
        {
            inputF.image.color = new Color32(243, 107, 122, 255);
            inputF.transform.localScale = new Vector3(1.379219f, 1.5125f, 1f);
        }

        GameObject[] controlButtonsGO = GameObject.FindGameObjectsWithTag("ControlButtons");
        Button controlButton = controlButtonsGO[2].GetComponent<Button>();

        for(int i=0; i < controlButtonsGO.Length; i++){
            controlButton = controlButtonsGO[i].GetComponent<Button>();
            if(controlButton.GetComponentInChildren<Text>().text == "This") break;
        }
        controlButton.image.sprite = pauseSprite;
        controlButton.image.rectTransform.sizeDelta = new Vector2(30,35);

        GameObject[] internetStatusGO = GameObject.FindGameObjectsWithTag("InternetStatus");
        InternetStatus internetStatus = internetStatusGO[0].GetComponent<InternetStatus>();

        if(internetStatus.internetStatus && !forceEnd){
            GameObject[] voteQPaneGO = GameObject.FindGameObjectsWithTag("VoteQPaneController");
            votingPlane voteQPane = voteQPaneGO[0].GetComponent<votingPlane>();
            voteQPane.move();
        }

        GameObject[] startDebateGO = GameObject.FindGameObjectsWithTag("StartDebate");
        StartDebate startDebate = startDebateGO[0].GetComponent<StartDebate>(); 
        startDebate.debateStarted = false;

        forceEnd = false;
    }
}
