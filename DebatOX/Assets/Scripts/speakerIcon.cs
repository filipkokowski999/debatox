using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class speakerIcon : MonoBehaviour
{
    bool muted;

    public Sprite speaker;
    public Sprite speakerMuted;

    public void changeIcon(){
        GameObject[] speakerGO = GameObject.FindGameObjectsWithTag("Speaker");
        Image speakerB = speakerGO[0].GetComponent<Image>();

        if(!muted){
            speakerB.sprite = speakerMuted;
            muted = !muted;
        }
        else {
            speakerB.sprite = speaker;
            muted = !muted;
        }

    }
}
