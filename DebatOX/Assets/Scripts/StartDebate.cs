using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class StartDebate : MonoBehaviour
{
public string code;
public bool debateStarted;
public void onClick()
{
        GameObject[] zaTematem = GameObject.FindGameObjectsWithTag("ZaTematem");
        GameObject[] PrzeciwkoTematowi = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        GameObject[] zaTematemDelete = GameObject.FindGameObjectsWithTag("DeleteZaTematem");
        GameObject[] PrzeciwkoTematowiDelete = GameObject.FindGameObjectsWithTag("DeletePrzeciwkoTematowi");

        GameObject[] AddButtons = GameObject.FindGameObjectsWithTag("AddButton");

        GameObject[] TematDebatyGO = GameObject.FindGameObjectsWithTag("TematDebaty");
        TMP_InputField TematDebaty = TematDebatyGO[0].GetComponent<TMP_InputField>();

        GameObject[] TimerGO = GameObject.FindGameObjectsWithTag("Czas");
        TMP_InputField TimerTF = TimerGO[0].GetComponent<TMP_InputField>();

        GameObject[] TimerChGO = GameObject.FindGameObjectsWithTag("CzasChroniony");

        GameObject[] TimerGOTxt = GameObject.FindGameObjectsWithTag("Timer");

        GameObject[] Hourglass = GameObject.FindGameObjectsWithTag("Hourglass");

        GameObject[] StartButton = GameObject.FindGameObjectsWithTag("StartButton");

        GameObject[] TimeButtons = GameObject.FindGameObjectsWithTag("CzasyButton");

        string timeInput = TimerTF.text;
        bool timeFormatGood = false;
        bool securedTimeFormatGood = false;
        int min = 0;
        int sec = 0;

        TMP_InputField securedTimeTxtField = TimerChGO[0].GetComponent<TMP_InputField>();

        string securedTimeString = securedTimeTxtField.text;
        int securedTimeSec = 0;

        string tytułString = "";

        foreach (char character in securedTimeString)
        {
            if (character == ':') securedTimeFormatGood = true;
        }
        if (securedTimeString == "")
        {
            securedTimeSec = 0;
            securedTimeFormatGood = true;
        }
        else if (securedTimeFormatGood)
        {
            string[] time = securedTimeString.Split(':');

            string minS = time[0];
            string secS = time[1];

            min = int.Parse(minS);
            sec = int.Parse(secS);

            securedTimeSec = min * 60 + sec;
        }

        timeFormatGood = false;

        foreach (char character in timeInput){
            if (character == ':') timeFormatGood = true;
        }
        if (timeFormatGood && securedTimeFormatGood) {
            string[] time = timeInput.Split(':');

            string minS = time[0];
            string secS = time[1];

            min = int.Parse(minS);
            sec = int.Parse(secS);

            if (min * 60 + sec > securedTimeSec)
            {
                if (min >= 9 && sec > 0) timeFormatGood = false;
                else if (min > 9) timeFormatGood = false;
                else if (sec >= 60) timeFormatGood = false;
            }
            else
            {
                timeFormatGood = false;
            }

            //TMP_Text Timer = TimerGO[1].GetComponent<TMP_Text>();
        }
    if (timeFormatGood && zaTematem.Length == PrzeciwkoTematowi.Length)
    {
        Debug.Log(min);
        Debug.Log(sec);

        TematDebaty.readOnly = true;

        foreach (GameObject textField in zaTematem)
        {
            TMP_InputField inputFieldBlue = textField.GetComponent<TMP_InputField>();
            inputFieldBlue.readOnly = true;
        }

        foreach (GameObject textField in PrzeciwkoTematowi)
        {
            TMP_InputField inputFieldRed = textField.GetComponent<TMP_InputField>();
            inputFieldRed.readOnly = true;
        }

        for (int i = 1; i < zaTematemDelete.Length; i++) zaTematemDelete[i].transform.localPosition += new Vector3(-300, 0,0);

        for (int i = 1; i < PrzeciwkoTematowiDelete.Length; i++) PrzeciwkoTematowiDelete[i].transform.localPosition += new Vector3(300, 0, 0); ;

        foreach (GameObject button in AddButtons)
        {
            Button AddButton = button.GetComponent<Button>();
            AddButton.transform.localPosition += new Vector3(0,700,0); //Przesuni�cie addbutton�w
        }

            Button Start = StartButton[0].GetComponent<Button>();
            Start.transform.localPosition += new Vector3(0,-300,0);
          
            GameObject[] TitleTxtField = GameObject.FindGameObjectsWithTag("TematDebaty");
            TMP_InputField tytułTxtField = TitleTxtField[0].GetComponent<TMP_InputField>();
            tytułString = tytułTxtField.text;

            GameObject[] TematTxt = GameObject.FindGameObjectsWithTag("Temat");
            TMP_Text tytułTxt = TematTxt[0].GetComponent<TMP_Text>();

            TematTxt[0].transform.localPosition = TitleTxtField[0].transform.localPosition;

            tytułTxt.text = tytułString;
            if(tytułString.Length > 100) tytułTxt.fontSize = 10;

            tytułTxt.fontSize = 12;

            if (tytułString.Length <= 64) TematTxt[0].transform.localPosition -= new Vector3(0,7.8f,0);

            //Destroy(TitleTxtField[0]);
            TitleTxtField[0].transform.localPosition += new Vector3(0,700,0);

            TimerGOTxt[0].transform.localPosition = new Vector3(-2.5f,-21,0);
            //TimerGO[0].SetActive(false);

            //Destroy(StartButton[0]);

            Timer timer = TimerGOTxt[0].AddComponent<Timer>();
            timer.values(min, sec, false, true, securedTimeSec, false, 0, 0, true, false, true, true, false, false);

            GameObject[] previousGO = GameObject.FindGameObjectsWithTag("Prev");
            previousPerson previous = previousGO[0].GetComponent<previousPerson>();
            previous.defaultTime(sec);

            //Destroy(TimeButtons[0]);
            //Destroy(TimeButtons[1]);

            TMP_InputField timerIF = TimerGO[0].GetComponent<TMP_InputField>();
            TMP_InputField timerChIF = TimerChGO[0].GetComponent<TMP_InputField>();

            Debug.Log(timerIF.text);
            Debug.Log(timerChIF.text);

            if (TimerGO[0].transform.localPosition.y < TimerChGO[0].transform.localPosition.y)
            {
                TimerGO[0].transform.localPosition += new Vector3(0, 200, 0);
                TimerChGO[0].transform.localPosition = TimerGO[0].transform.localPosition;
            }
            else
            {
                TimerChGO[0].transform.localPosition += new Vector3(0, 200, 0);
                TimerGO[0].transform.localPosition = TimerChGO[0].transform.localPosition;
            }
            TimeButtons[0].transform.localPosition += new Vector3(0, 200, 0);
            TimeButtons[1].transform.localPosition += new Vector3(0, 200, 0);

        code = RandomStringGenerator(6);

        GameObject[] internetStatusGO = GameObject.FindGameObjectsWithTag("InternetStatus");
        InternetStatus internetStatus = internetStatusGO[0].GetComponent<InternetStatus>();

        if(internetStatus.internetStatus){
            StartCoroutine(GetRequest("https://debatox.live/unity_db/addToDB.php?kod=" + code + "&tytul=" + tytułString));
        }

        debateStarted = true;

        GameObject[] codeTxtGo = GameObject.FindGameObjectsWithTag("CodeTxt");
        Text codeTxt = codeTxtGo[0].GetComponent<Text>();

        codeTxt.text = code;
    }
}

string RandomStringGenerator(int lenght)
{
    string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567890!#$%*!#$%*";
    string generated_string = "";

    for(int i = 0; i < lenght; i++) generated_string += characters[Random.Range(0, characters.Length - 1)];

    return generated_string;
}

IEnumerator GetRequest(string uri)
{
    using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
    {
        // Request and wait for the desired page.
        yield return webRequest.SendWebRequest();

        string[] pages = uri.Split('/');
        int page = pages.Length - 1;

        switch (webRequest.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                break;
            case UnityWebRequest.Result.ProtocolError:
                Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                break;
            case UnityWebRequest.Result.Success:
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                break;
        }
    }
}

}
