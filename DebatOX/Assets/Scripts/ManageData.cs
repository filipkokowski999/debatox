using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeTime : MonoBehaviour
{
    public string textInput;
    public TMP_InputField InputField;
    public TMP_InputField TextDisplay;

    public void StoreName()
    {
        textInput = InputField.text;
        TextDisplay.text = textInput;
    }
}
