using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class votingPlane : MonoBehaviour
{
    public bool soundsOn = true;

    public void move(){
        GameObject[] VoteQPane = GameObject.FindGameObjectsWithTag("VoteQPane");

        if(VoteQPane[0].transform.localPosition.y != -2){
            VoteQPane[0].transform.localPosition = new Vector3(0,-2,0);
        }
        else{
            VoteQPane[0].transform.localPosition = new Vector3(0,700,0);
        }
    }   

    public void yes(){
        move();
        GameObject[] votingPane = GameObject.FindGameObjectsWithTag("VotingPane");
        votingPane[0].transform.localPosition = new Vector3(0,1,0);

        GameObject[] internetStatusGO = GameObject.FindGameObjectsWithTag("InternetStatus");
        InternetStatus internetStatus = internetStatusGO[0].GetComponent<InternetStatus>();

        if(internetStatus.internetStatus){
            GameObject[] votesGO = GameObject.FindGameObjectsWithTag("reloadVotes");
            votingResults votes = votesGO[0].GetComponent<votingResults>();
            votes.setCode();
            votes.started = true;
        }
    }

    public void no(){
        move();
    }

    public void endVoting(){
        GameObject[] startDebateGO = GameObject.FindGameObjectsWithTag("StartDebate");
        StartDebate startDebate = startDebateGO[0].GetComponent<StartDebate>(); 

        StartCoroutine(GetRequest("https://debatox.live/unity_db/deleteFromDB.php?kod=" + startDebate.code));

        GameObject[] votingPane = GameObject.FindGameObjectsWithTag("VotingPane");
        votingPane[0].transform.localPosition -= new Vector3(0,700,0);
    }

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }

}

