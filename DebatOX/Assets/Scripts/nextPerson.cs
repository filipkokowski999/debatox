using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class nextPerson : MonoBehaviour
{
    Timer next;

    public Sprite play;
    public void onClick()
    {
        GameObject[] nextGO = GameObject.FindGameObjectsWithTag("Timer");
        next = nextGO[0].GetComponent<Timer>();
        next.nextPerson = true;
        next.looped = false;
        next.saveTime = true;
        next.prev = true;
        next.pause = true;

        GameObject[] controlButtons = GameObject.FindGameObjectsWithTag("ControlButtons");
        Button controlButton = controlButtons[2].GetComponent<Button>();

        for(int i=0; i < controlButtons.Length; i++){
            controlButton = controlButtons[i].GetComponent<Button>();
            if(controlButton.GetComponentInChildren<Text>().text == "This") break;
        }
        controlButton.image.rectTransform.sizeDelta = new Vector2(30,35);
        controlButton.image.sprite = play;
        
        if(next.timeValue == 0) next.playS = true;
        else next.playS = false;

        next.skipped++;
        //Debug.Log("next.lastLoop: " + next.lastLoop);
        //Debug.Log("next.count: " + next.countLoop);
        if(!next.lastLoop) next.loopF();
        else Destroy(next);
        
    }
}
