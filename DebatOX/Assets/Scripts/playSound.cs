using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playSound : MonoBehaviour
{

    public AudioSource question;
    public AudioSource time;

    public void playSoundQuestion()
    {
        question.Play();
    }

    public void playSoundTime()
    {
        time.Play();
    }
}
