using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Debate : MonoBehaviour
{

    public void debateProcess(int seconds, int index, bool propozycja, bool lastLoop, bool prev)
    {
        //strona = false - poparcie, strona = true - opozycja

        GameObject[] poparcieGO = GameObject.FindGameObjectsWithTag("ZaTematem");
        GameObject[] opozycjaGO = GameObject.FindGameObjectsWithTag("PrzeciwkoTematowi");

        TMP_InputField[] poparcie = new TMP_InputField[poparcieGO.Length];
        TMP_InputField[] opozycja = new TMP_InputField[opozycjaGO.Length];

        Color32 blue = new Color32(146, 170, 255, 255);
        Color32 blue2 = new Color32(104, 135, 246, 255);
        Color32 red = new Color32(243, 107, 122, 255);
        Color32 red2 = new Color32(200, 58, 74, 255);

        for (int i = 0; i < poparcieGO.Length; i++)
        {
            poparcie[i] = poparcieGO[i].GetComponent<TMP_InputField>();
            poparcie[i].image.color = blue;
            poparcie[i].transform.localScale = new Vector3(1.38f, 1.51f, 1);
        }
        Debug.Log(poparcie[0].transform.localScale);

        for (int i = 0; i < opozycjaGO.Length; i++)
        {
            opozycja[i] = opozycjaGO[i].GetComponent<TMP_InputField>();
            opozycja[i].image.color = red;
            opozycja[i].transform.localScale = new Vector3(1.38f, 1.51f, 1);
        }

        if (lastLoop || index >= poparcie.Length + opozycja.Length)
        {
            opozycja[opozycja.Length-1].image.color = red;
            opozycja[opozycja.Length - 1].transform.localScale -= new Vector3(.1f, .1f, 0);

            Debug.Log("To już jest koniec nie ma już nic");

            GameObject[] endGO = GameObject.FindGameObjectsWithTag("EndDebate");
            endDebate end = endGO[0].GetComponent<endDebate>();
            end.resetScene();
        }
        else if (index == 0)
        {
            if(!prev){
                Debug.Log("NotPrev");
                poparcie[0].image.color = blue2;
                poparcie[0].transform.localScale += new Vector3(.1f, .1f, 0);
            }
            else{
                Debug.Log("Prev");
                poparcie[0].image.color = blue;
                poparcie[0].transform.localScale = new Vector3(1.38f, 1.51f, 1);
                opozycja[1].image.color = red2;
                opozycja[1].transform.localScale += new Vector3(.1f, .1f, 0);
            }
        }
        else
        {
            if (!propozycja)
            {
                if(!prev){
                    Debug.Log("NotPrev");
                    opozycja[(index - 1) / 2].image.color = red2;
                    opozycja[(index - 1) / 2].transform.localScale += new Vector3(.1f, .1f, 0);

                    poparcie[(index - 1) / 2].image.color = blue;
                    poparcie[(index - 1) / 2].transform.localScale = new Vector3(1.38f, 1.51f, 1);
                }
                else{
                    Debug.Log("Prev");
                    opozycja[(index + 1) / 2].image.color = red;
                    opozycja[(index + 1) / 2].transform.localScale = new Vector3(1.38f, 1.51f, 1);

                    poparcie[(index - 1) / 2].image.color = blue2;
                    poparcie[(index - 1) / 2].transform.localScale += new Vector3(.1f, .1f, 0);
                    
                }
            }
            else
            {
                if(!prev){
                    Debug.Log("NotPrev");
                    poparcie[index/2].image.color = blue2;
                    poparcie[index/2].transform.localScale += new Vector3(.1f, .1f, 0);

                    opozycja[(index-2) / 2].image.color = red;
                    opozycja[(index-2) / 2].transform.localScale = new Vector3(1.38f, 1.51f, 1);
                }
                else{
                    Debug.Log("Prev");
                    poparcie[index/2].image.color = blue;
                    poparcie[index/2].transform.localScale = new Vector3(1.38f, 1.51f, 1);

                    opozycja[(index-2) / 2].image.color = red2;
                    opozycja[(index-2) / 2].transform.localScale += new Vector3(.1f, .1f, 0);
                }
            }
        }
    }


}
