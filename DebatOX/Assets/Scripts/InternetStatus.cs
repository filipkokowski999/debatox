 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;


public class InternetStatus : MonoBehaviour
{
    public Sprite InternetConnection, noInternetConnection;
    GameObject[] InternetStatusImageGO;
    Image InternetStatusImage;
    GameObject[] NoDataImageGO;
    public bool internetStatus = true;

    Color disabledButton = new Color32(121,121,121,255);
    Color enabledButton = new Color32(179,179,179,255);

    GameObject[] startDebateGO;
    StartDebate startDebate;

    Ping ping;
    List<int> pingList;

    bool pingDone = true;

    float waitUntilNextCheck = 0;
    void Start()
    {
        InternetStatusImageGO = GameObject.FindGameObjectsWithTag("InternetIcon");
        InternetStatusImage = InternetStatusImageGO[0].GetComponent<Image>();

        NoDataImageGO = GameObject.FindGameObjectsWithTag("NoDataIcon");

        startDebateGO = GameObject.FindGameObjectsWithTag("StartDebate");
        startDebate = startDebateGO[0].GetComponent<StartDebate>(); 

        GetRequest("google.com");
    }

    void Update()
    {
        StartCoroutine(GetRequest("google.com"));
        Debug.Log("parapapapa");
        if(internetStatus)
        {
            InternetStatusImage.sprite = InternetConnection;
            InternetStatusImageGO[0].transform.localScale = new Vector3(1,1.2f,1);

            GameObject[] InternetIconMenuGO = GameObject.FindGameObjectsWithTag("InternetIconMenu");
            InternetIconMenuGO[0].transform.localScale = new Vector3(0,0,0);

            GameObject[] MenuGO = GameObject.FindGameObjectsWithTag("Menu");
            Debug.Log("Menu length: " + MenuGO.Length);

            if(startDebate.debateStarted){
                GameObject[] VoteButtonGO = GameObject.FindGameObjectsWithTag("VoteButton");
                Button VoteButton = VoteButtonGO[0].GetComponent<Button>();
                VoteButton.enabled = true;
                VoteButton.image.color = enabledButton;
                NoDataImageGO[0].transform.localScale = new Vector3(0,0,0);
            }
            else{
                GameObject[] VoteButtonGO = GameObject.FindGameObjectsWithTag("VoteButton");
                Button VoteButton = VoteButtonGO[0].GetComponent<Button>();
                VoteButton.enabled = false;
                VoteButton.image.color = disabledButton;
                NoDataImageGO[0].transform.localScale = new Vector3(2.7f,1.72f,1);
            }
    
        }
        else{
            InternetStatusImage.sprite = noInternetConnection;
            InternetStatusImage.color = disabledButton;
            InternetStatusImageGO[0].transform.localScale = new Vector3(1,1.2f,1);

            GameObject[] InternetIconMenuGO = GameObject.FindGameObjectsWithTag("InternetIconMenu");
            InternetIconMenuGO[0].transform.localScale = new Vector3(2.31f,1.72f,1);

            if(startDebate.debateStarted){
                GameObject[] VoteButtonGO = GameObject.FindGameObjectsWithTag("VoteButton");
                Button VoteButton = VoteButtonGO[0].GetComponent<Button>();
                VoteButton.enabled = false;
                VoteButton.image.color = disabledButton;
                NoDataImageGO[0].transform.localScale = new Vector3(0,0,0);
            }
            else{
                GameObject[] VoteButtonGO = GameObject.FindGameObjectsWithTag("VoteButton");
                Button VoteButton = VoteButtonGO[0].GetComponent<Button>();
                VoteButton.enabled = false;
                VoteButton.image.color = disabledButton;
                NoDataImageGO[0].transform.localScale = new Vector3(2.7f,1.72f,1);
            }
        }
    }

    IEnumerator GetRequest(string uri)
    {
        waitUntilNextCheck += Time.deltaTime;
        if(waitUntilNextCheck >= 3){
            waitUntilNextCheck = 0;
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                string[] pages = uri.Split('/');
                int page = pages.Length - 1;

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                        internetStatus = false; 
                        break;
                        
                    default:
                        internetStatus = true;  
                        break;
                }
            }
        }
    }
    
}
