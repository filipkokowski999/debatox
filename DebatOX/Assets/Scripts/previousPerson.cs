using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class previousPerson : MonoBehaviour
{
    public Sprite playSprite;
    public int[] minutesArr;
    public int[]secondsArr;
    public bool[] loopArr;
    public bool[] firstPlayArr;
    public int[] securedTimeArr;
    public bool[] loopedArr;
    public int[] countArr;
    public int[] indexArr;
    public bool[] propozycjaArr;
    public bool[] lastLoopArr;
    public bool[] playSArr;
    public bool[] isSet;
    public bool[] soundPlayedArr;

    void Start(){
        minutesArr = new int[11];
        secondsArr = new int[11];
        loopArr = new bool[11];
        firstPlayArr = new bool[11];
        securedTimeArr = new int[11];
        loopedArr = new bool[11];
        countArr = new int[11];
        indexArr = new int[11];
        propozycjaArr = new bool[11];
        lastLoopArr = new bool[11];
        playSArr = new bool[11];
        isSet = new bool[11];
        soundPlayedArr = new bool[11];
    }

    public void defaultTime(int seconds){ 
        for(int i=0; i < 11; i++) secondsArr[i] = seconds; 
        for(int i=1; i < 11; i++) indexArr[i] = -1;
    }

    public void addData(int minutes, int seconds, bool loop, bool firstPlay, int securedTime, bool looped, int count, int index, bool propozycja, bool lastLoop, bool playS, bool soundPlayed){
        minutesArr[index] = minutes;
        secondsArr[index] = seconds;
        loopArr[index] = loop;
        firstPlayArr[index] = firstPlay;
        securedTimeArr[index] = securedTime;
        loopedArr[index] = looped;
        countArr[index] = count;
        indexArr[index] = index;
        propozycjaArr[index] = propozycja;
        lastLoopArr[index] = lastLoop;
        playSArr[index] = playS;
        soundPlayedArr[index] = soundPlayed;
    }

    public void changeTime(int index, int seconds, bool soundPlayedTimer){
        secondsArr[index - 1] = seconds;
        isSet[index] = true;
        soundPlayedArr[index - 1] = soundPlayedTimer;
    }

    public void prevPerson(){
        GameObject[] timerGOClass = GameObject.FindGameObjectsWithTag("Timer");
        Timer timerClass = timerGOClass[0].GetComponent<Timer>();
        if(timerClass.index - 1 == 0){}
        else if(timerClass.index > 0){
            int currentIndex = timerClass.index - 1;

            foreach (var comp in timerClass.GetComponents<Component>())
            {
                if (comp is Timer) Destroy(comp);
            }
    
            GameObject[] TimerGOTxt = GameObject.FindGameObjectsWithTag("Timer");
            Timer timer = TimerGOTxt[0].AddComponent<Timer>();
            //timer.values(minutesArr[currentIndex - 2], secondsArr[currentIndex - 2], loopArr[currentIndex - 2], firstPlayArr[currentIndex - 2], securedTimeArr[currentIndex - 2], loopedArr[currentIndex - 2], countArr[currentIndex - 2], indexArr[currentIndex - 2], propozycjaArr[currentIndex - 2], lastLoopArr[currentIndex - 2], playSArr[currentIndex - 2]);
            timer.values(minutesArr[currentIndex - 1], secondsArr[currentIndex - 1], loopArr[currentIndex - 1], firstPlayArr[currentIndex - 1], securedTimeArr[currentIndex - 1], loopedArr[currentIndex - 1], countArr[currentIndex - 1], indexArr[currentIndex - 1], propozycjaArr[currentIndex - 1], lastLoopArr[currentIndex - 1], playSArr[currentIndex - 1], timer.soundsOn, true, soundPlayedArr[currentIndex - 1]);
            timer.prev = true;

            GameObject[] controlButtonsGO = GameObject.FindGameObjectsWithTag("ControlButtons");
            Button controlButton = controlButtonsGO[2].GetComponent<Button>();

            for(int i=0; i < controlButtonsGO.Length; i++){
                controlButton = controlButtonsGO[i].GetComponent<Button>();
                if(controlButton.GetComponentInChildren<Text>().text == "This") break;
            }
            controlButton.image.sprite = playSprite;
            controlButton.image.rectTransform.sizeDelta = new Vector2(30,35);

            if(timer.saveTime){
                GameObject[] previousctGO = GameObject.FindGameObjectsWithTag("Prev");
                previousPerson previousct = previousctGO[0].GetComponent<previousPerson>();
                previousct.changeTime(timer.index, timer.sec, timer.soundPlayed);
                Debug.Log("index:" + timer.index + ":" + timer.sec);
                timer.saveTime = false;
            }
        }
    }
}
